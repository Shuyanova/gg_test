/**
 * Created on 21.12.15.
 */

$(document).ready(function(){
    $('select').each(function(){
        select_stylized($(this));
    });
    $('select').change(function(){
        select_stylized($(this));
    });
});

/**
$('#rect3085').hover(function(){
    $(this).attr('style', 'fill:#000;fill-opacity:1;stroke:none');
}, function(){
    $(this).attr('style', 'fill:#4f5977;fill-opacity:1;stroke:none');
});
 */


$(document).ready(function() {
    height_control();
});

$(document).resize(function() {
    height_control();
});

function height_control(){

    /*if (jQuery(window).width() < 992){
        return false;
    }*/

    var content_height = jQuery('.donate').height();
    var nav_height = jQuery('.status').height();
    var result_height = content_height

    if (content_height > nav_height){
        result_height = content_height;
    }else{
        result_height = nav_height
    }

    jQuery('.status').css({
        'height': result_height + 69 + 'px'
    });
    jQuery('.donate').css({
        'height': result_height + 69 + 'px'
    });

}

function select_stylized(selector) {
    selector.siblings('p').text(selector.children('option:selected').text());
}